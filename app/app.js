var butandingApp    =   angular.module("butandingApp", [
    'ngRoute',
    'butanding.modules.home',
    'butanding.modules.packages',
]);

butandingApp.controller("main", function ($scope) {
    $scope.baseURL = window.location.origin + window.location.pathname + '#/';
});