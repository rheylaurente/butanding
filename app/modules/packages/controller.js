var packages    =   angular.module("butanding.modules.packages", []);

// configure routes
packages.config(function ($routeProvider) {
    $routeProvider.when('/packages', {
        templateUrl: 'app/modules/packages/index.html',
        controller: 'butandingPackagesInit'
    });
});

packages.controller("butandingPackagesInit", function ($scope) {
    
});