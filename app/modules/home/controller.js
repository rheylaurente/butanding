var home    =   angular.module("butanding.modules.home", []);

// configure routes
home.config(function ($routeProvider) {
    $routeProvider.when('/', {
        templateUrl: 'app/modules/home/index.html',
        controller: 'butandingHomeInit'
    });
});


home.controller("butandingHomeInit", function ($scope) {
    var labels  =   [
        "Adventure",
        "Explore",
        "Relax"
    ];
    $scope.label    =   labels[0];
    $('#home-backgrounds').on('slide.bs.carousel', function () {
        var key =   labels.indexOf($scope.label);
        key++;
        if(typeof(labels[key])==='undefined'){
            key =   0;
        }
        $scope.label    =   labels[key];
        $scope.$apply();
    });
});