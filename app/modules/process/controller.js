var process    =   angular.module("butanding.modules.process", []);

// configure routes
packages.config(function ($routeProvider) {
    $routeProvider.when('/process', {
        templateUrl: 'app/modules/process/index.html',
        controller: 'butandingProcess'
    });
});

packages.controller("butandingProcess", function ($scope) {

        $scope.requestCountry = function(callback){
             $http.get('http://api.geonames.org/countryInfoJSON?username=demo')
                .then(function(response){
                       callback.success(response.data.geonames);
               },$scope.onError);
        };
               
    
});